module CtciMergeSort where

type Arr = (Int, [Int])

merge :: Arr -> Arr -> Arr
merge (n1, acc1) (n2, acc2) = (n + n1 + n2, acc) where
    (n, acc) =  helper (length acc1) acc1 acc2 [] 0
    helper :: Int ->  [Int] -> [Int] -> [Int] -> Int -> Arr
    helper l [] ys zs n = (n, (reverse zs)++ys)
    helper l xs [] zs n = (n, (reverse zs)++xs)
    helper l xs'@(x:xs) ys'@(y:ys) zs n = if x <= y then helper (pred l) xs ys' (x:zs) n
                                                    else helper l xs' ys (y:zs) (n + l)

split :: [Int] -> ([Int], [Int])
split xs = helper xs [] xs where
    helper :: [Int] -> [Int] -> [Int] -> ([Int], [Int])
    helper (x1:x2:xs) ys (z:zs)  = helper xs (z:ys) zs
    helper _ ys zs = (reverse ys, zs)

mergeSort :: [Int] -> Arr
mergeSort [] = (0, [])
mergeSort [x] = (0, [x])
mergeSort xs = merge arr1' arr2' where
    (arr1, arr2) = split xs
    arr1' = mergeSort arr1
    arr2' = mergeSort arr2

processCase :: IO Arr
processCase = do
    n <- readLn :: IO Int
    str <- getLine
    let arr = map read $ words str :: [Int]
    return $ mergeSort arr

main :: IO ()
main = do
    d <- readLn :: IO Int
    ans <- sequenceA $ replicate d processCase
    putStr $ unlines (map (show . fst) ans)
