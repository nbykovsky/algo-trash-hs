-- problem https://www.hackerrank.com/challenges/convex-hull-fp/problem
module ConvexHull where

import Data.List

type Point = (Double, Double)

-- split string to pair
strToPair:: String -> Point
strToPair str = (x, y) where x:y:_ = map read $ words str

-- find point with minimum y coordinate
start :: [Point] -> Point
start = foldl1 helper where
    helper (x1, y1) (x2, y2) | (y1 < y2)||(y1 == y2 && x1 < x2) = (x1, y1)
                             | otherwise = (x2, y2)

-- calculate distance between two points
dist :: Point -> Point -> Double
dist (x1, y1) (x2, y2) = sqrt $ (x1-x2)**2 + (y1-y2)**2

-- calculate cosine of angle between vector and x axis
cosine :: Point -> Point -> Double
cosine (xp, yp) (x, y) = (x - xp) / dist (xp, yp) (x, y)


data Turn = LeftTurn | RightTurn deriving Show

-- determine Left or Right turn. forward is right turn
turn :: Point -> Point -> Point -> Turn
turn (x1, y1) (x2, y2) (x3, y3) =
    let t = (x2-x1)*(y3-y1)-(y2-y1)*(x3-x1)
       in if t > 0 then LeftTurn
          else RightTurn

-- sort by angle between x axis and start-point vector and distance to start
sorted :: Point -> [Point] -> [Point]
sorted st = sortBy (\p1 p2 -> compare (cs p2, ds p1) (cs p1, ds p2))
    where cs = cosine st
          ds = dist st

getPath :: [Point] -> Point -> [Point] -> [Point]
getPath [] p [] = [p]
getPath rs p [] = p:rs
getPath [] p (q:qs) = getPath [p] q qs
getPath (r:rs) p (q:qs) =
    case turn r p q of
        LeftTurn -> getPath (p:r:rs) q qs
        RightTurn -> getPath rs r (q:qs)

perimeter :: [Point] -> Double
perimeter pairs = calcDist (head pairs) (head pairs) (tail pairs) 0
    where calcDist :: Point -> Point -> [Point] -> Double -> Double
          calcDist st mid [] acc = acc + dist st mid
          calcDist st mid (x:xs) acc = calcDist st x xs (acc + dist mid x)

main :: IO ()
main = do
    n <- readLn :: IO Int
    ls <- sequenceA $ replicate n getLine
    let points = map strToPair ls
    let start' = start points
    let filtered = sorted start' $ filter (/= start') points
    let path = getPath [start'] (head filtered) (tail filtered)
    print $ perimeter path
