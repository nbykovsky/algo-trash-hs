--Problem
--The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.
--There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.
--How many circular primes are there below one million?

module PrimeNumbers where

import Data.List

primesTo :: Int -> [Int]
primesTo m = sieve [2..m]
             where
             sieve (x:xs) = x : sieve (xs \\ [x,x+x..m])
             sieve [] = []

rotateOne :: Int -> Int -> Int
rotateOne n _ = n `div` 10 + n `rem` 10 * 10 ^ (pred $ length $ show n)

rotateMany :: Int -> [Int]
rotateMany n = scanl rotateOne n [2..length $ show n]

checkAll:: [Int] -> [Int] -> Bool
checkAll candidate base = all (\x -> elem x base) candidate

check :: Int -> [Bool]
check n = map (\x -> checkAll (rotateMany x) er) er
    where er = primesTo n

fnl :: Int -> Int
fnl n = sum $ map (\x -> if x then 1 else 0) (check n)

--The same solution but compressed
--p m=s[2..m]where s(x:y)=x:s(y\\[x,x+x..m]);s[]=[]
--f n=l$filter id$map(\x->all(flip elem b)(scanl(\m _->m`div`10+m`rem`10*10^(pred$a m))x[2..a x]))b where a m=l$show m;b=p n;l=length