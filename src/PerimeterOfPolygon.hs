-- task https://www.hackerrank.com/challenges/lambda-march-compute-the-perimeter-of-a-polygon/problem
module PerimeterOfPolygon where

type Point = (Double, Double)

strToPair:: String -> Point
strToPair str = (x, y) where x:y:_ = map read $ words str

dist :: Point -> Point -> Double
dist (x1, y1) (x2, y2) = sqrt $ (x1-x2)**2 + (y1-y2)**2

calcDist :: Point -> Point -> [Point] -> Double -> Double
calcDist st mid [] acc = acc + dist st mid
calcDist st mid (x:xs) acc = calcDist st x xs (acc + dist mid x)

main :: IO ()
main = do
    n <- readLn :: IO Int
    ls <- sequenceA $ replicate n getLine
    let pairs = map strToPair ls
    let distance = calcDist (head pairs) (head pairs) (tail pairs) 0
    print distance
