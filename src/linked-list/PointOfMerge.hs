-- problem
-- https://www.hackerrank.com/challenges/find-the-merge-point-of-two-joined-linked-lists/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=linked-lists
module PointOfMerge where

data LinkedList a =  Cons a (LinkedList a) | Nil deriving (Eq, Show)

myLength :: LinkedList a -> Int
myLength Nil = 0
myLength (Cons _ xs) = succ $ myLength xs

myTake :: Int -> LinkedList a -> LinkedList a
myTake 0 xs = xs
myTake _ Nil = error "Empty List"
myTake n (Cons _ xs) = myTake (pred n) xs

mergePoint :: Eq a => LinkedList a -> LinkedList a -> Maybe a
mergePoint Nil _ = Nothing
mergePoint _ Nil = Nothing
mergePoint ll1 ll2 =
    let offset = myLength ll1 - myLength ll2
        ll1' = myTake (max offset 0) ll1
        ll2' = myTake (max ((-1)*offset) 0) ll2
        helper :: Eq a => LinkedList a -> LinkedList a -> Maybe a
        helper (Cons x xs) (Cons y ys) | x == y && xs == ys = Just x
                                       | otherwise = helper xs ys
        helper _ _ = Nothing
    in helper ll1' ll2'

list0 :: LinkedList Int
list0 = Cons 2 (Cons 1 (Cons 0 Nil))

list1 :: LinkedList Int
list1 =  Cons 10 (Cons 11 list0)

list2 :: LinkedList Int
list2 =  Cons 21 list0

list2':: LinkedList Int
list2' = Cons 21 Nil

-- mergePoint list1 list2
-- mergePoint list1 list2'