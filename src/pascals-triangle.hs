-- problem https://www.hackerrank.com/challenges/pascals-triangle/problem
module Main where
import Data.List

row :: Int -> [Int]
row 1 = [1]
row k = [1] ++ (zipWith (+) (row $ k - 1) $ tail $ row $ k - 1) ++ [1]

triangle :: Int -> [[Int]]
triangle k = map row [1..k]

output :: [[Int]] -> String
output = intercalate "\n" . (map $ intercalate " " . map show)

main :: IO ()
main = do
    line <- getLine
    let k = (read line)::Int
        in putStrLn $ output . triangle $ k
    return ()